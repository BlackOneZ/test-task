import React from 'react';

interface ItemProps {
  item: { id: number; name: string };
  onSelect: (item: { id: number; name: string }) => void;
  isSelected: boolean;
}

const ItemComponent: React.FC<ItemProps> = ({ item, onSelect, isSelected }) => {
  return (
    <div
      onClick={() => onSelect(item)}
      style={{ border: isSelected ? '2px solid blue' : '1px solid black', padding: '10px', margin: '5px', cursor: 'pointer' }}
    >
      {item.name}
    </div>
  );
};

export default ItemComponent;
