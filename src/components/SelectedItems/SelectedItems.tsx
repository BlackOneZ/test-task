import React from 'react';

interface SelectedItemsProps {
  items: { id: number; name: string }[];
}

const SelectedItems: React.FC<SelectedItemsProps> = ({ items }) => {
  return (
    <div className={"top__list"}>
      {items.map(item => (
        <div key={item.id} className={"top__item"}>
          {item.name}
        </div>
      ))}
    </div>
  );
};

export default SelectedItems;
