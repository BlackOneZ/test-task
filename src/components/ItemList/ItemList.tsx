import React, { useState } from 'react';
import ItemComponent from '../ItemComponent/ItemComponent';

interface ItemListProps {
  items: { id: number; name: string }[];
  maxSelect: number;
  isSingleSelect: boolean;
  onSelectionChange: (selectedItems: { id: number; name: string }[]) => void;
}

const ItemList: React.FC<ItemListProps> = ({ items, maxSelect, isSingleSelect, onSelectionChange }) => {
  const [selectedItems, setSelectedItems] = useState<{ id: number; name: string }[]>([]);

  const handleSelect = (item: { id: number; name: string }) => {
    if (isSingleSelect) {
      const newSelection = selectedItems.includes(item) ? [] : [item];
      setSelectedItems(newSelection);
      onSelectionChange(newSelection);
    } else {
      if (selectedItems.includes(item)) {
        const newSelection = selectedItems.filter(i => i.id !== item.id);
        setSelectedItems(newSelection);
        onSelectionChange(newSelection);
      } else if (selectedItems.length < maxSelect) {
        const newSelection = [...selectedItems, item];
        setSelectedItems(newSelection);
        onSelectionChange(newSelection);
      }
    }
  };

  return (
    <div className={'bottom__list'}>
      {items.map(item => (
        <ItemComponent
          key={item.id}
          item={item}
          onSelect={handleSelect}
          isSelected={selectedItems.includes(item)}
        />
      ))}
    </div>
  );
};

export default ItemList;
