import { useState } from 'react'
import './App.css'
import ItemList from './components/ItemList/ItemList'
import SelectedItems from './components/SelectedItems/SelectedItems'
import { firstData, secondData } from './data/data'

function App() {
  const [firstSelectedItems, setFirstSelectedItems] = useState<{ id: number; name: string }[]>([]);
  const [secondSelectedItem, setSecondSelectedItem] = useState<{ id: number; name: string } | null>(null);

  return (
    <>
      <div className='top'>
        <SelectedItems items={firstSelectedItems} />
        <SelectedItems items={secondSelectedItem ? [secondSelectedItem] : []} />
      </div>
      <div className='bottom'>
        <ItemList 
          items={firstData.flat()} 
          maxSelect={6} 
          isSingleSelect={false}
          onSelectionChange={setFirstSelectedItems} 
        />
        <ItemList 
          items={secondData.flat()} 
          maxSelect={1} 
          isSingleSelect={true}
          onSelectionChange={(items) => setSecondSelectedItem(items[0] || null)} 
        />
      </div>
    </>
  )
}

export default App
